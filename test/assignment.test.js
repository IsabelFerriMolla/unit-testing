const expect = require('chai').expect;
const should = require('chai').should();
const{assert} = require('chai');
const mymodule = require('../assignmentUnitTesting/moduleAssignment.js');


describe('Assignment for Unit testing Isabel Ferri Mollá, moduleAssignment.js', () =>{
    let myvar= undefined
    before(()=>{ 
        variable=parseInt(Math.random()*(30 - 1) + 1);
       
        console.log("before test...");
        console.log("the variable value is... "+variable);})
    it('variable should exist',() => {
        should.exist(variable); // we expect true
             
 })
    it('When it uses maximum function with a=3 and b=7 it should return 7',() => {
        const result = mymodule.max(3,7);  //7>3
        expect(result).to.equal(7);   // we expect that the resuld would be 7
    }) 
    
    it('assert max(12,24) is 24',() => {
        assert(mymodule.max(12,24) == 24); //true
            
})

    it('When it uses factorial function with a=5 it should return 120',() => {
        const result = mymodule.factorial(5);  //5!=120
        expect(result).to.equal(120);   // we expect that the resuld would be 120
    }) 
    


    it('parametrized factorial testing',() => {
        const result = parseInt(mymodule.factorial(variable)); // we do the factorial but we only get the integer part
      var  res=1;
        for( i =1;i<=variable; i++){
           res=res*i

        }
        res=parseInt(res);
        expect(result).to.equal(res);
    })  

        it('assert hello is not bye',() => {
           assert('hello' !== 'bye'); //true
        })
          it('assert fact 3 is 6',() => {
            assert(mymodule.factorial(3) == 6); //true
                
    })
    after(()=>{
        console.log("after test...");
        var exsquare= variable*variable;
        console.log("the square of the variable is "+ exsquare )
        console.log(`correct? ${exsquare==(variable**2)}`);
        
        variable =0;
        console.log("now the variable value is... "+variable);
    })

    })