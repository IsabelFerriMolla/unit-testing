

const expect = require('chai').expect;
const should = require('chai').should();
const{assert} = require('chai');
const mylib = require('../src/mylib');


describe('Unit testing mylib.js', () =>{
    let myvar= undefined
    before(()=>{ myvar=1;
    console.log("before testing...");})
    it('my var should exist',() => {
        should.exist(myvar); //true
             
 })
    it('Should retoutn 2 when using sum function when a=1,b=1',() => {
        const result = mylib.sum(1,1);//1+1
        expect(result).to.equal(2);// result expected to be =2
    }) 
    
    it('parametrized way of unit testing',() => {
        const result = mylib.sum(myvar,myvar);//1+1
        expect(result).to.equal(myvar+myvar);
    })  

        it('assert foo is not bar',() => {
           assert('foo' !== 'bar'); //true
                
    })
    after(()=>{
        console.log("after testing...");})
    })