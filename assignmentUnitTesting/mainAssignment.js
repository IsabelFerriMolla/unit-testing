const express = require('express');
const app= express();
const port=3000;
const mymodule = require('./moduleAssignment');

app.get('/', (req, res) => {res.send('Unit Testing Assignment');});

app.get('/max', (req, res)=> {
    const a =parseInt(req.query.a);
    const b =parseInt(req.query.b);
    var maxResult=mymodule.max(a, b);
   
   
    res.send(`max's result= ${maxResult}`);
 
});

app.get('/factorial', (req, res)=> {
    const a =parseInt(req.query.a);
    var factResult=mymodule.factorial(a)
  
   
    res.send(`factorial's result= ${factResult}`);
});

console.log({ max:mymodule.max(1, 3) ,
   fact:mymodule.factorial(3)});
   


app.listen(port,() => { console.log(`Server:http://localhost:${port}`);});
