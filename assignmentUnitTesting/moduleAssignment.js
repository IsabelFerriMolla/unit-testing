//assignmentUnitTesting/moduleAssignment.js
module.exports = {
    max:(a, b) => {
        if(a>=b) return a;
        else return b;
    },

    factorial:(a) =>{
      var aux =a;
      var res =1;
      while (aux>1){
          res=res*aux;
          aux--;
      }
      return res;

    }

}